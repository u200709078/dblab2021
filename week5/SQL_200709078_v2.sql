use movie_db;
# 1. Show the films whose budget is greater than 10 million$ and ranking is less than 6.
select * 
from movies 
where budget>10000000 and ranking<6;

# 2. Show the action films whose rating is greater than 8.8 and produced before 2009.
select *
from movies join genres on movies.movie_id = genres.movie_id
where rating>8.8 and year<2009 and genre_name="Action";

# 3. Show the drama films whose duration is more than 150 minutes and oscars is more than 2.
# Solution 1
select * from movies join genres on movies.movie_id = genres.movie_id
where duration>150 and oscars>2 and genre_name="Drama";

# Solution 2
select * from movies
where duration>150 and oscars>2 and movie_id in
(
	select movie_id
	from genres
	where genre_name="Drama"
);

# 4. Show the films that Orlando Bloom and Ian McKellen have act together and has more than 2 Oscars.
select * from movies
inner join movie_stars on movies.movie_id=movie_stars.movie_id
inner join stars on movie_stars.star_id=stars.star_id
where (stars.star_name="Orlando Bloom" or stars.star_name="Ian McKellen") and movies.oscars>2;

# 5. Show the Al Pacino (Quentin Tarantino) films which have more than 500000 votes and produced before 1975 (2000).
select * from movies
inner join movie_stars on movies.movie_id=movie_stars.movie_id
inner join stars on movie_stars.star_id=stars.star_id
where stars.star_name="Al Pacino" and movies.votes>500000 and movies.year<1975;
 
# 6. Show the thriller films whose budget is greater than 25 million$.
select * from movies join genres on movies.movie_id = genres.movie_id
where movies.budget>25000000 and genres.genre_name="Thriller";

# 7. Show the drama films whose language is Italian and produced between 1990-2000.	
select * from movies join genres on movies.movie_id = genres.movie_id
where genres.genre_name="Drama" and movies.year>=1990 and movies.year<=2000 and movies.movie_id in
(
select movie_id from languages
where language_name="Italian"
);

# 8. Show the films that Tom Hanks has act and have won more than 3 Oscars.	 
select * from movies
inner join movie_stars on movies.movie_id=movie_stars.movie_id
inner join stars on movie_stars.star_id=stars.star_id
where stars.star_name="Tom Hanks" and movies.rating>3;

# 9. Show the history films produced in USA and whose duration is between 100-200 minutes.
select * from movies
inner join genres on movies.movie_id = genres.movie_id
inner join producer_countries on movies.movie_id = producer_countries.movie_id
inner join countries on producer_countries.country_id = countries.country_id
where genres.genre_name="History" and countries.country_name = "USA" and 100<movies.duration<200;

# 10.Compute the average budget of the films directed by Peter Jackson.
select avg(movies.budget) as "The average budget of the films directed by Peter Jackson"  from movies 
inner join movie_directors on movies.movie_id = movie_directors.movie_id
inner join directors on movie_directors.director_id=directors.director_id
where directors.director_name="Peter Jackson";

# 11.Show the Francis Ford Coppola film that has the minimum budget.
select min(movies.budget) as "The Francis Ford Coppola film that has the minimum budget"  from movies 
inner join movie_directors on movies.movie_id = movie_directors.movie_id
inner join directors on movie_directors.director_id=directors.director_id
where directors.director_name="Francis Ford Coppola";

# 12.Show the film that has the most vote and has been produced in USA.
select max(movies.rating) as "The film that has the most vote and has been produced in USA" from movies
inner join producer_countries on movies.movie_id = producer_countries.movie_id
inner join countries on countries.country_id = producer_countries.country_id
where countries.country_name = "USA";