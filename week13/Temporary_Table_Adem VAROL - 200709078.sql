-- Adem VAROL - 200709078

drop temporary table IF EXISTS product_below_avg;

create temporary table product_below_avg (temp_id integer not null auto_increment primary key)
select ProductID, ProductName, Price
from products
where price < (select avg(price) from products);

select * from product_below_avg;

show tables;
drop temporary table product_below_avg;

