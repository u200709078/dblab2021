## Adem VAROL - 200709078
## WARNING!!!

select * from customers;
## UPDATE - SET
update customers set country=replace(country,'\n','');
update customers set city=replace(city,'\n','');

## VIEW
create view mexicanCustomers as
select CustomerID, CustomerName, ContactName 
from customers where Country="Mexico";

select * from mexicancustomers;

## USE VIEW
select * 
from mexicancustomers join orders on mexicancustomers.CustomerID=orders.CustomerID;

select * from products;

create view productsbelowavg as
select productid, productname, price
from products
where price < (select avg(price) from products);

## DELETE & TRUNCATE
## delete from orderdetails where ProductID=5;

select * from orderdetails where ProductID=5;
truncate orderdetails;

delete from customers;
delete from orders;

drop table customers;

